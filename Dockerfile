FROM node:lts AS frontend-builder
WORKDIR /build
COPY . /build
RUN npm install
RUN npm run build

FROM node:lts AS frontend-runner
COPY --from=frontend-builder /build/dist .
EXPOSE 8888
ENTRYPOINT ["npx", "-y", "http-server-spa", ".", "index.html", "8888"]