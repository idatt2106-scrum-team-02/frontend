describe('Cannot register with wrong fields', () => {
    it('passes', () => {
      cy.visit('http://localhost:4173/registerAccount')
      cy.get('#register-button').trigger('click')
      cy.get('#error-message').contains("Vennligst fyll inn alle feltene")
      
      cy.get('#email-input').type('en ugyldig epost')
      cy.get('#register-button').trigger('click')
      cy.get('#error-message').contains("Vennligst fyll inn alle feltene")
  
      cy.get('#password-input').type('hei')
      cy.get('#register-button').trigger('click')
      cy.get('#error-message').contains("Vennligst fyll inn alle feltene")
    })
  
  })