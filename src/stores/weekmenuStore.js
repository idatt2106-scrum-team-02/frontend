import { defineStore } from "pinia";

export const useWeekMenuStore = defineStore("weekmenu", {
    state: () => {
        return {
            items: []
        };
    },
    persist: {
        storage: localStorage
    },
    getters: {
        getItems() {
            return this.items;
        },
        getItem(state){
            return(id) => state.items.filter(item =>
                item.id === id)
        }
    },
    actions: {
        reset() {
            this.$reset();
        },
        addWeekmenu(item){
            this.items.push(item);
        }
    }
});