import { defineStore } from "pinia";
export const useAuthStore = defineStore("auth", {
  state: () => {
    return {
      token: "",
      account: {},
      profile: {},
      profiles: [],
      items: {ingredientList: [], suggestionList: []},
      fridgeItems: [],
    };
  },
  persist: {
    storage: localStorage
  },
  getters: {
    isLoggedIn() {
      return this.token.length > 0
    }
  },
  actions: {
    setToken(token) {
      this.token = token;
    },
    setAccount(account) {
      this.account = account;
    },
    logout() {
      this.$reset();
    },
    setProfile(profile) {
      this.profile = profile;
    },
    setProfiles(profiles) {
      this.profiles = profiles;
    },
    setItems(items) {
      this.items = items;
    },
    setItem(item) {
      this.items.push(item);
    },
    setFridge(fridgeItems){
      this.fridgeItems = fridgeItems;
    },
    addItemToFridge(fridgeItem){
      this.fridgeItems.push(fridgeItem);
    },
    updateProfile(name, image, isRestricted){
      this.profile.name = name;
      this.profile.profileImageUrl = image;
      this.profile.restricted = isRestricted;
    }
  }
});
