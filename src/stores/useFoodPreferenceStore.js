import {API} from "@/util/API"
import { defineStore} from "pinia";

export const useFoodPreferenceStore = defineStore("foodPreference", {
    state: () => {
        return {
            foodPreferences: [],
        };
    },
    persist: {
        storage: localStorage,
    },
    actions: {
        fetchAllOptions() {
            API.getFoodpreferences()
                .then((foodPreferences) => {
                    this.foodPreferences = foodPreferences;
                })
        }
    }
})