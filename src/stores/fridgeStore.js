import { defineStore } from "pinia";

export const useFridgeStore = defineStore("fridge", {
    state: () => {
        return {
            items: []
        };
    },
    persist: {
        storage: localStorage
    },
    getters: {
        getItems() {
            return this.items;
        },
        getItem(state){
            return(id) => state.items.filter(item =>
            item.id === id)
        }
    },
    actions: {
        reset() {
            this.$reset();
        },
        addToFridge(item){
            this.items.push(item);
        }
    }
});