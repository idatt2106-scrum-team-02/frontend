import { createRouter, createWebHistory } from 'vue-router'
import ProfileSettings from "@/views/SettingsView.vue";
import MissingPage from "@/views/MissingPage.vue";
import HomeView from '../views/HomeView.vue'
import LoginView from '../views/LoginView.vue'
import SelectProfileView from '../views/SelectProfileView.vue'
import ProfileCreationView from '../views/ProfileCreationView.vue'
import RegisterAccountView from '../views/RegisterAccountView.vue'
import WeekMenu from '../views/WeekMenuView.vue'
import PinCodeView from "@/views/PinCodeView.vue";
import FridgeView from "@/views/FridgeView.vue";
import RecipeView from "@/views/RecipeView.vue";

import ShoppingListView from '../views/ShoppingListView.vue'
import { useAuthStore } from "@/stores/authStore.js";

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeView
    },
    {
      path: '/login',
      name: 'login',
      component: LoginView
    },
    {
      path: '/selectProfile',
      name: "selectProfile",
      component: SelectProfileView
    },
    {
      path: '/newProfile',
      name: "newProfile",
      component: ProfileCreationView
    },
    {
      path: '/registerAccount',
      name: 'registerAccount',
      component: RegisterAccountView
    },
    {
      path: '/pincode',
      name: 'pincode',
      component: PinCodeView
    },
    {
      path: '/myFridge',
      name: 'myFridge',
      component: FridgeView
    },
    {
      path: '/weekmenu',
      name: 'weekmenu',
      component: WeekMenu
    },
    {
      path: '/recipe/:id',
      name: 'recipe',
      component: RecipeView
    },
    {
      path: '/shoppingList',
      name: 'shoppingList',
      component: ShoppingListView
    },
    {
      path: '/profileSettings',
      name: 'profileSettings',
      component: ProfileSettings
    },
    {
      path: '/:catchAll(.*)',
      name: "404 Page not found" ,
      component: MissingPage 
    }
  ]
})

router.beforeEach (async (to, from) => {
  const authStore = useAuthStore();
  if (!authStore.isLoggedIn && to.name !== 'login' && to.name !== 'registerAccount') {
    return { name: 'login'};
  }
})


export default router
