import { describe, it, expect } from 'vitest'
import { mount } from '@vue/test-utils'
import ItemSearch from "@/components/ItemSearch.vue";

const SearchReturn = [
    {
        "id": "1",
        "ean": "7040512550214",
        "name":"eplekake",
        "amount": {"quantity": "4","unit": "stk"},
        "image_url": "https://bilder.ngdata.no/7040512550818/meny/large.jpg"
    },
    {
        "id": "2",
        "ean": "7040512550254",
        "name":"eple",
        "amount": {"quantity": "4","unit": "stk"},
        "image_url": "https://bilder.ngdata.no/7040512550818/meny/large.jpg"
    }];

describe('Behaves as expected', () => {
    it('shows correct number of results', () => {
        const wrapper = mount(ItemSearch, {
            data() {
                return {
                    searchResult: SearchReturn,
                    selectedItem: SearchReturn[0],
                    numOfItemsToAdd:3,
                };
            },
        });

        expect(wrapper.text()).toContain('Resultater: ')
        expect(wrapper.text()).toContain('(2)')
    });

    it('When adding more items, the total ingredient amount is shown', () => {
        const wrapper = mount(ItemSearch, {
            data() {
                return {
                    searchResult: SearchReturn,
                    selectedItem: SearchReturn[0],
                    numOfItemsToAdd:3,
                };
            },
        });

        // total = 3*4
        const totalItemCount = wrapper.find('span')
        expect(totalItemCount.text()).toContain('12')
        expect(totalItemCount.text()).toContain('stk')
    });

    it('Select contains all items ', () => {
        const wrapper = mount(ItemSearch, {
            data() {
                return {
                    searchResult: SearchReturn,
                    selectedItem: SearchReturn[0],
                    numOfItemsToAdd:3,
                };
            },
        });
        const itemSelect = wrapper.find('select')
        expect(itemSelect.text()).toContain('eple')
        expect(itemSelect.text()).toContain('eplekake')
    });
})
