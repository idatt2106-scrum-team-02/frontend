import { describe, it, expect } from 'vitest'

import { mount } from '@vue/test-utils'
import Navbar from "@/components/Navbar.vue";
import { Icon } from '@iconify/vue';
import { RouterLink } from 'vue-router';

describe('Navbar', () => {
    it('contains 5 links', () => {
        const wrapper = mount(Navbar)
        expect(wrapper.findAll('RouterLink').length).toBe(5)
    })

    it('contains 5 icons', () => {
        const wrapper = mount(Navbar)
        const icons = wrapper.findAllComponents(Icon)
        expect(icons).toHaveLength(5)})
})
