import { mount } from '@vue/test-utils'
import { describe, it, expect, vi } from 'vitest'
import { createTestingPinia } from '@pinia/testing'
import {useAuthStore} from "@/stores/authStore";
import EditProfile from '@/components/EditProfile.vue'
describe('EditProfile', () => {
    const pinia = createTestingPinia({
        initialState: {
            profile: { name: 'Ola',restricted:false,profileImageUrl:"some/valid/image.png" },
        },
        createSpy: vi.fn(),
    })

    const wrapper = mount(EditProfile, {
        global: {
            plugins: [pinia],
        },
    })

    const store = useAuthStore(pinia)
    store.profile = {
        name:"Ola",
        restricted:true,
        profileImageUrl:"some/valid/image.png"
    }

    it('Profile name is on profile page', () => {
        expect(wrapper.vm.updatedProfile.upName).toContain('Ola')
        const unameInput = wrapper.find('#brukernavn');
        expect(unameInput.element.value).to.contain('Ola');
    })

    it('If profile.restricted is true, then radio input with value false is not selected', () => {
        const radioInput = wrapper.find('input[type=radio][value="false"]')
        expect(radioInput.element.checked).toBe(false)
    })

    it('If profile.restricted is true, then radio input with valuetrue *is* selected', () => {
        const radioInput = wrapper.find('input[type=radio][value="true"]')
        expect(radioInput.element.checked).toBe(true)
    })

    //update the value from restricted true -> false
    it('After changing restricted radio, the values are updated too', async () => {
        const notRestrictedRadioInput = wrapper.find('#normal')
        const restrictedRadioInput = wrapper.find('#restricted')

        expect(notRestrictedRadioInput.element.checked).toBe(false)
        expect(restrictedRadioInput.element.checked).toBe(true)

        await notRestrictedRadioInput.trigger('click')

        expect(notRestrictedRadioInput.element.checked).toBe(true)
        expect(restrictedRadioInput.element.checked).toBe(false)

        await wrapper.vm.$nextTick()

        setTimeout(() => {
            expect(wrapper.vm.updatedProfile.upRestricted).toBe(false)
        }, 1000);
    })


})