import { describe, it, expect } from 'vitest'
import { mount } from '@vue/test-utils'
import FridgeItem from "@/components/FridgeItem.vue";

const normalItem = {
    "item":{
        "id": "1",
        "name":"eple",
        "amount": {"quantity": "4","unit": "stk"},
        "image_url": "https://bilder.ngdata.no/7040512550818/meny/large.jpg"
    },
    "exp_date": "2222-02-22T00:00:00+00:00",
    "amount": {"quantity": "6","unit": "stk"}
}

const expiredItem = {
    "item":{
        "id": "1",
        "name":"eple",
        "amount": {"quantity": "4","unit": "stk"},
        "image_url": "https://bilder.ngdata.no/7040512550818/meny/large.jpg"
    },
    "exp_date": "2002-02-22T00:00:00+00:00",
    "amount": {"quantity": "6","unit": "stk"}
}
describe('Fridge items render correctly', () => {

    it('displays the name of the item', () => {
        const wrapper = mount(FridgeItem, {
            props: {
                fridgeItem: normalItem,
            },
        });
        expect(wrapper.text()).toContain('eple');
    });

    it('displays the amount of the item in the fridge' , () => {
        const wrapper = mount(FridgeItem, {
            props: {
                fridgeItem: normalItem,
            },
        });
        expect(wrapper.text()).toContain('6');
        expect(wrapper.text()).toContain('stk');
        });

    it('displays item image', () => {
        const wrapper = mount(FridgeItem, {props: {
                fridgeItem: normalItem,
            },
        });
        const itemImage = wrapper.find('img');
        expect(itemImage.exists()).toBe(true);
        expect(itemImage.element.getAttribute('src')).not.toBe('');
        expect(itemImage.element.getAttribute('src')).toBe('https://bilder.ngdata.no/7040512550818/meny/large.jpg');
    });

    it('displays text of different color when item has expired', () => {
        const wrapper = mount(FridgeItem, {
            props: {
                fridgeItem: expiredItem,
            },
        });

        const expiration = wrapper.find('.expText');
        expect(expiration.element.style.color).not.toBe('black');
    });

})
describe('Behaves as expected', () => {
    it('emits when the apple button is pressed', async () => {
        const wrapper = mount(FridgeItem, {
            props: {
                fridgeItem: normalItem,
            },
        });

        await wrapper.find('#appleBtn').trigger('click');
        await wrapper.vm.$nextTick();
        expect(wrapper.emitted().appleBtnPressed).toBeTruthy();

    })
})
