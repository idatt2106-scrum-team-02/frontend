import {describe, it, expect, vi} from 'vitest'
import { mount } from '@vue/test-utils'
import EditAccount from "@/components/EditAccount.vue";
import {createTestingPinia} from "@pinia/testing";
import {useAuthStore} from "@/stores/authStore";
describe('Behaves as expected', () => {
    const wrapper = mount(EditAccount, {
        global: {
            plugins: [createTestingPinia({
                createSpy: vi.fn,
            })],
        },
    });

    const store = useAuthStore()

    store.account = {
        id: "1",
        email:"epost@epost.no",
        firstname:"Ola",
        password: "Ola123",
        fridge: {},
    }

    it('Has email field that contains the account email', async () => {
        expect(wrapper.find('#emailField').text()).toContain('epost@epost.no');
    })

    it('Has firstname field with current firstname', async () => {
        expect(wrapper.vm.updatedAccount.upFirstname).to.equal('Ola');
        const fnameInput = wrapper.find('#fname');
        expect(fnameInput.element.value).to.equal('Ola');
    })

    it('Password field is empty', async () => {
        const passwordInput = wrapper.find('#password');
        expect(passwordInput.element.value).to.equal('');
    })

    it('attempting to delete account without checking box results in error message', async () => {
        await wrapper.find('#delAccount').trigger('click');
        const alertMsg = wrapper.vm.delAlertMsg;
        expect(alertMsg).to.contain('boks');

    })

})
