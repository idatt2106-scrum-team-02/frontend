import { describe, it, expect } from 'vitest'
import { mount } from '@vue/test-utils'
import EatFridgeItemModal from "@/components/EatFridgeItemModal.vue";

const normalItem = {
    "item":{
        "id": "1",
        "name":"purre",
        "amount": {"quantity": "500","unit": "g"},
        "image_url": "https://bilder.ngdata.no/7040512550818/meny/large.jpg"
    },
    "exp_date": "2222-02-22T00:00:00+00:00",
    "amount": {"quantity": "500","unit": "g"}
}

const expiredItem = {
    "item":{
        "id": "1",
        "name":"purre",
        "amount": {"quantity": "500","unit": "g"},
        "image_url": "https://bilder.ngdata.no/7040512550818/meny/large.jpg"
    },
    "exp_date": "2002-02-22T00:00:00+00:00",
    "amount": {"quantity": "500","unit": "g"}
}
describe('Renders correctly', () => {
    it('displays the name of the item', () => {
        const wrapper = mount(EatFridgeItemModal, {
            props: {
                fridgeItem: normalItem,
            },
        });

        const itemName = wrapper.find('h2')
        expect(itemName.text()).toContain('purre')

    });

    it('displays the correct unit', () => {
        const wrapper = mount(EatFridgeItemModal, {
            props: {
                fridgeItem: normalItem,
            },
        });

        const sliderValueUnit = wrapper.find('#sliderDisplay')
        expect(sliderValueUnit.text()).toContain('g')

    });

    it('Displays a message if item has expired', () => {
        const wrapper = mount(EatFridgeItemModal, {
            props: {
                fridgeItem: expiredItem,
            },
        });

        expect(wrapper.find('#itemHasExpiredMessage').exists()).toBe(true);
        const expiredMsgBox = wrapper.find('#itemHasExpiredMessage')
        expect(expiredMsgBox.isVisible()).toBe(true)

    });

    it('Does not display message if item has not expired', () => {
        const wrapper = mount(EatFridgeItemModal, {
            props: {
                fridgeItem: normalItem,
            },
        });
        expect(wrapper.find('#itemHasExpiredMessage').exists()).toBe(false);
    });

})

describe('Behaves correctly', () => {
    it('emits close when X is pressed', async () => {
        const wrapper = mount(EatFridgeItemModal, {
            props: {
                fridgeItem: normalItem,
            },
        });

        await wrapper.find('#exitBtn').trigger('click');
        await wrapper.vm.$nextTick();
        expect(wrapper.emitted().closeModal).toBeTruthy();

    });
})