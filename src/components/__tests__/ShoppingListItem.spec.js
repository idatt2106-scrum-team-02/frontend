import { describe, it, expect, vi} from 'vitest'
import { createTestingPinia } from '@pinia/testing'
import { mount } from '@vue/test-utils'
import ShoppingListItem from "@/components/ShoppingListItem.vue";



describe('ShoppingListItem', () => {
    it('mounts correctly', () => {
        const wrapper = mount(ShoppingListItem, {
            global: {
              plugins: [createTestingPinia({
                createSpy: vi.fn,
              })],
            },
            props: {
              itemName: "Test",
              amount: 99
            }
          })
        expect(wrapper.text()).toMatch("99x Test")
    })
})
