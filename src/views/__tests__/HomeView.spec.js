import { describe, it, expect, vi } from 'vitest'
import { createTestingPinia } from '@pinia/testing'

import { mount } from '@vue/test-utils'
import HomeView from '../HomeView.vue'

describe('Home', () => {
  it('renders properly', () => {
    const wrapper = mount(HomeView, {
        global: {
          plugins: [createTestingPinia({
            createSpy: vi.fn,
          })],
        },
      })
    expect(wrapper.text()).toContain('Velkommen,')
  })

  it('tips renders properly', () => {
    const wrapper = mount(HomeView, {
        global: {
          plugins: [createTestingPinia({
            createSpy: vi.fn,
          })],
        },
      })
    expect(wrapper.find('#tips-text').text()).not.toBe("");
  })
})