import { describe, it, expect, vi } from 'vitest'
import { createTestingPinia } from '@pinia/testing'
import { mount } from '@vue/test-utils'
import FridgeView from "@/views/FridgeView.vue";


const fridgeIngredients = [
    {
        "item":{
            "id": "1",
            "name":"eple",
            "amount": {"quantity": "4","unit": "stk"},
            "image_url": "https://bilder.ngdata.no/7040512550818/meny/large.jpg"
        },
        "exp_date": "2222-02-22T00:00:00+00:00",
        "amount": {"quantity": "6","unit": "stk"}
    }];

describe('Fridge', () => {
    it('renders', () => {
        const wrapper = mount(FridgeView, {
            global: {
                plugins: [createTestingPinia({
                    createSpy: vi.fn,
                })],
            },
        })
        expect(wrapper.text()).toContain('Kjøleskap')
    });

    it('pressing the plus button makes search visible', async () => {
        const wrapper = mount(FridgeView, {
            global: {
                plugins: [createTestingPinia({
                    createSpy: vi.fn,
                })],
            },

        })
        expect(wrapper.find('ItemSearch').exists()).toBe(false);
        await wrapper.find('.add-button').trigger('click');

        setTimeout(() => {
            expect(wrapper.find('ItemSearch').exists()).toBe(true);
        }, 1000);
    });

    it('when appleBtnEmit, the EatFridgeItemModal is displayed', () => {
        const wrapper = mount(FridgeView, {
            global: {
                plugins: [createTestingPinia({
                    createSpy: vi.fn,
                })],
            },
            data() {
                return {
                    fridgeItems: fridgeIngredients,
                };
            },

        })

        expect(wrapper.find('eat-fridge-item-modal').exists()).toBe(false);

        wrapper.find('#appleBtn').trigger('click');
        setTimeout(() => {
            expect(wrapper.find('eat-fridge-item-modal').exists()).toBe(true);
        }, 1000);
    })
})