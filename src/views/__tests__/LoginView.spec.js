import { describe, it, expect } from 'vitest'

import { mount } from '@vue/test-utils'
import LoginView from '../LoginView.vue'

describe('Login', () => {
  it('renders properly', () => {
    const wrapper = mount(LoginView)
    expect(wrapper.text()).toContain('E-post')
    expect(wrapper.text()).toContain('Passord')
  })

  it('login button exists', () => {
    const wrapper = mount(LoginView)
    expect(wrapper.find('#login-button').exists()).toBe(true);
  })
})