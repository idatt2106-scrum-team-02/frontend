import { describe, it, expect } from 'vitest'

import { mount } from '@vue/test-utils'
import RegisterAccountView from '../RegisterAccountView.vue'

describe('Login', () => {
  it('renders properly', () => {
    const wrapper = mount(RegisterAccountView)
    expect(wrapper.text()).toContain('E-post')
    expect(wrapper.text()).toContain('Navn')
    expect(wrapper.text()).toContain('Passord')
  })

  it('register button exists', () => {
    const wrapper = mount(RegisterAccountView)
    expect(wrapper.find('#register-button').exists()).toBe(true)
  })
})