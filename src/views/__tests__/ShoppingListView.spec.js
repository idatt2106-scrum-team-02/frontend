import { describe, it, expect, vi} from 'vitest'
import { mount } from '@vue/test-utils'
import { createTestingPinia } from '@pinia/testing'
import ShoppingListView from '../ShoppingListView.vue'


  
  describe('Shopping list', () => {
    const wrapper = mount(ShoppingListView, 
        { computed: {
            items() {
                return {
                    id: 1,
                    ingredientList: [
                    {
                        ingredient_id: 1,
                        amount: {
                        quantity: 1,
                        unit: "stk"
                        },
                        item: {
                        id: 1,
                        name: "Vegansk Krone-Is Jordbær",
                        ean: "7041012550001",
                        shelfLife: 30,
                        image_url: "https://bilder.ngdata.no/7041012550001/meny/large.jpg",
                        amount: {
                            quantity: 4,
                            unit: "stk"
                        },
                        store: null,
                        allergens: [
                            {
                            name: "Nøtter (kan inneholde spor)"
                            },
                            {
                            name: "Peanøtter (kan inneholde spor)"
                            },
                            {
                            name: "Soya (kan inneholde spor)"
                            }
                        ],
                        nutrition: [
                            {
                            name: "Kalorier",
                            amount: {
                                quantity: 264,
                                unit: "stk"
                            }
                            },
                            {
                            name: "Energi",
                            amount: {
                                quantity: 1106,
                                unit: "stk"
                            }
                            },
                            {
                            name: "Fett",
                            amount: {
                                quantity: 11.2,
                                unit: "g"
                            }
                            },
                            {
                            name: "Karbohydrater",
                            amount: {
                                quantity: 38.7,
                                unit: "g"
                            }
                            },
                            {
                            name: "Mettet fett",
                            amount: {
                                quantity: 9.5,
                                unit: "g"
                            }
                            },
                            {
                            name: "Protein",
                            amount: {
                                quantity: 1.5,
                                unit: "g"
                            }
                            },
                            {
                            name: "Salt",
                            amount: {
                                quantity: 0.11,
                                unit: "g"
                            }
                            },
                            {
                            name: "Sukkerarter",
                            amount: {
                                quantity: 23.6,
                                unit: "g"
                            }
                            }
                        ]
                        },
                        exp_date: null
                    },
                    {
                        ingredient_id: 2,
                        amount: {
                        quantity: 1,
                        unit: "stk"
                        },
                        item: {
                        id: 2,
                        name: "Jarlsberg Gulost",
                        ean: "7038010053368",
                        shelfLife: 14,
                        image_url: "https://bilder.ngdata.no/7038010053368/kmh/large.jpg",
                        amount: {
                            quantity: 700,
                            unit: "g"
                        },
                        store: null,
                        allergens: [
                            {
                            name: "Melk"
                            }
                        ],
                        nutrition: [
                            {
                            name: "Kalorier",
                            amount: {
                                quantity: 351,
                                unit: "kcal"
                            }
                            },
                            {
                            name: "Energi",
                            amount: {
                                quantity: 1458,
                                unit: "kj"
                            }
                            },
                            {
                            name: "Fett",
                            amount: {
                                quantity: 27,
                                unit: "g"
                            }
                            },
                            {
                            name: "Karbohydrater",
                            amount: {
                                quantity: 0,
                                unit: "g"
                            }
                            },
                            {
                            name: "Mettet fett",
                            amount: {
                                quantity: 17,
                                unit: "g"
                            }
                            },
                            {
                            name: "Protein",
                            amount: {
                                quantity: 27,
                                unit: "g"
                            }
                            },
                            {
                            name: "Salt",
                            amount: {
                                quantity: 1.1,
                                unit: "g"
                            }
                            },
                            {
                            name: "Sukkerarter",
                            amount: {
                                quantity: 0,
                                unit: "g"
                            }
                            }
                        ]
                        },
                        exp_date: null
                    }
                    ],
                    suggestionList: []
                }    
            },
            profile() {
                return {
                    
                    id: 1,
                    email: "ola@ola.no",
                    firstname: "Ola",
                    shoppingList: {
                        id: 1,
                        ingredientList: [],
                        suggestionList: []
                    },
                    fridge: {
                        id: 1,
                        ingredientList: []
                    },
                    username: "ola@ola.no",
                    authorities: [],
                    accountNonExpired: true,
                    accountNonLocked: true,
                    credentialsNonExpired: true,
                    enabled: true
                    }
                }
            },
            global: {
                plugins: [createTestingPinia({
                    createSpy: vi.fn,
                })],
                },
        
        })

  it('renders properly', () => {

    expect(wrapper.text()).toContain('Handleliste')
    expect(wrapper.text()).not.toContain('Forslag')
  })

  it('items gets shown', () => {
    expect(wrapper.text()).toContain('Vegansk Krone-Is Jordbær')
    expect(wrapper.text()).toContain('Jarlsberg Gulost')
  })

  it('pressing the plus button makes search visible', async () => {
    expect(wrapper.find('ItemSearch').exists()).toBe(false);
    await wrapper.find('.add-button').trigger('click');

    setTimeout(() => {
        expect(wrapper.find('ItemSearch').exists()).toBe(true);
    }, 1000);
});

})
