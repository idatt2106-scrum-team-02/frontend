import { describe, it, expect, vi } from 'vitest'
import { createTestingPinia } from '@pinia/testing'
import { mount } from '@vue/test-utils'
import SelectProfileView from '../SelectProfileView.vue'

describe('Select profile', () => {
  it('renders properly', () => {
    const wrapper = mount(SelectProfileView, {
      global: {
        plugins: [createTestingPinia({
          createSpy: vi.fn,
        })],
      },
    })
    expect(wrapper.text()).toContain('Hvem bruker appen?')
  })

  it('loads with one profile', () => {
    const wrapper = mount(SelectProfileView, {
        global: {
          plugins: [createTestingPinia({
            createSpy: vi.fn,
          })],
        },
        data() {
          return {
            profiles: [{
                id: -1,
                name: "test",
                profileImageUrl: "",
            }]
          }
        }
      })
    expect(wrapper.text()).toContain("test")

  })
})